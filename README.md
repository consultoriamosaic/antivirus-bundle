# Mosaic Antivirus Bundle

This bundle provides single files virus scan. Currently it uses VirusTotal API, but another
providers can be integrated.

## Instalation

    composer require "mosaic/antivirus-bundle"
    
Register the bundle in `app/AppKernel.php`:

``` php
public function registerBundles()
{
    return array(
        // ...
        new Antivirus\Infrastructure\AntivirusInfrastructureBundle(),
    );
}
```

Configuration
-------------

Include your VirusTotal API key in paramters.yml 

    virus_total_api_key: YOUR_VIRUS_TOTAL_API_KEY
  
Configure the path where files will be placed for scan:

``` yaml
antivirus_infrastructure:
    scan_directory: "%kernel.root_dir%/../var/files/scan"
```

Configure doctrine mappings

```yaml
antivirus_domain:
    type: yml
    dir: "%kernel.root_dir%/../vendor/mosaic/antivirus-bundle/src/Antivirus/Infrastructure/Persistence/Mapping/Domain/Antivirus"
    prefix: Antivirus\Domain\Antivirus
    is_bundle: false
antivirus_infrastructure:
    type: yml
    dir: "%kernel.root_dir%/../vendor/mosaic/antivirus-bundle/src/Antivirus/Infrastructure/Persistence/Mapping/Infrastructure"
    prefix: Antivirus\Infrastructure
    is_bundle: false
```

Finally execute migrations

    php bin/console doctrine:migrations:diff
    php bin/console doctrine:migrations:migrate


Symfony Commands
--------

Enqueue a single file

    antivirus:queue:enqueue path/to/file
    
Scan enqueued files asynchronously

    antivirus:queue:async-scan
    

DDD Integration
---------------

To enqueue a single file dispatch VirusScanRequested event
    
```php
DomainEventPublisher::instance()->publish(new VirusScanRequested(...));
```
    
To handle virus scan completion, subscribe to VirusScanCompleted event


```php
class VirusScanCompletedSubscriber implements DomainEventSubscriberInterface
{
    ...
    
    /**
     * @param DomainEventInterface $event
     * @return bool
     */
    public function isSubscribedTo(DomainEventInterface $event)
    {
        return VirusScanCompleted::class === get_class($event);
        }
    
    ...
}
```
