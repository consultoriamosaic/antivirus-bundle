<?php

namespace Antivirus\Domain\Antivirus;

use Mosaic\Common\DDD\Event\DomainEventInterface;

class VirusScanRequested implements DomainEventInterface
{
    /**
     * @var string
     */
    private $filename;

    /**
     * @var \DateTimeImmutable
     */
    private $occurredOn;

    /**
     * @param string $fileName
     */
    public function __construct($fileName)
    {
        $this->filename = $fileName;
        $this->occurredOn = new \DateTimeImmutable();
    }

    /**
     * @return string
     */
    public function filename()
    {
        return $this->filename;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function occurredOn()
    {
        return $this->occurredOn;
    }
}
