<?php

namespace Antivirus\Domain\Antivirus;

class VirusScanResult
{
    /**
     * @var bool
     */
    private $infected;

    /**
     * @param bool $infected
     */
    private function __construct($infected)
    {
        $this->infected = $infected;
    }

    /**
     * @return static
     */
    public static function buildInfectedFileScanResult()
    {
        return new static(true);
    }

    /**
     * @return static
     */
    public static function buildNotInfectedFileScanResult()
    {
        return new static(false);
    }

    /**
     * @return bool
     */
    public function hasVirus()
    {
        return $this->infected;
    }
}
