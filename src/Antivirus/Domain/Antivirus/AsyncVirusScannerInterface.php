<?php

namespace Antivirus\Domain\Antivirus;

interface AsyncVirusScannerInterface
{
    /**
     * @param File $file
     * @return string
     */
    public function scanFileAsync(File $file);

    /**
     * @param File $file
     * @return VirusScanResult
     */
    public function getScanResult(File $file);
}
