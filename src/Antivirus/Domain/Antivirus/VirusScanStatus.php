<?php

namespace Antivirus\Domain\Antivirus;

class VirusScanStatus
{
    const NOT_SCANNED = 0;
    const SCAN_STARTED = 1;
    const SCANNED = 2;
    const SCAN_FAILED = 3;

    /**
     * @var int
     */
    private $virusScanStatus;

    /**
     * @param $virusScanStatus
     */
    private function __construct($virusScanStatus)
    {
        $this->virusScanStatus = $virusScanStatus;
    }

    /**
     * @return VirusScanStatus
     */
    public static function buildNotScanned()
    {
        return new self(self::NOT_SCANNED);
    }

    /**
     * @return VirusScanStatus
     */
    public static function buildScanStarted()
    {
        return new self(self::SCAN_STARTED);
    }

    /**
     * @return VirusScanStatus
     */
    public static function buildScanned()
    {
        return new self(self::SCANNED);
    }

    /**
     * @return VirusScanStatus
     */
    public static function buildFailed()
    {
        return new self(self::SCAN_FAILED);
    }

    /**
     * @return bool
     */
    public function isScanned()
    {
        return $this->equals(VirusScanStatus::buildScanned());
    }

    /**
     * @return bool
     */
    public function isNotScanned()
    {
        return $this->equals(VirusScanStatus::buildNotScanned());
    }

    /**
     * @param VirusScanStatus $virusScanStatus
     * @return bool
     */
    public function equals($virusScanStatus)
    {
        return $this->virusScanStatus === $virusScanStatus->virusScanStatus;
    }
}
