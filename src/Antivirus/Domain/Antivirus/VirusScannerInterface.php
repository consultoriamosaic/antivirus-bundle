<?php

namespace Antivirus\Domain\Antivirus;

interface VirusScannerInterface
{
    /**
     * @param File $file
     * @return VirusScanResult
     */
    public function scanFile(File $file);
}
