<?php

namespace Antivirus\Domain\Antivirus;

interface FileRepositoryInterface
{
    /**
     * @param File $file
     */
    public function enqueue(File $file);

    /**
     * @param File $file
     */
    public function save(File $file);

    /**
     * @return File
     * @throws EmptyFileQueueException
     */
    public function nextFileForScan();

    /**
     * @param int $scanTimeInSeconds
     * @return File
     */
    public function nextFileForReport($scanTimeInSeconds);

    /**
     * @return int
     */
    public function scanQueueLength();

    /**
     * @return int
     */
    public function reportQueueLength();
}
