<?php

namespace Antivirus\Domain\Antivirus;

use Mosaic\Common\DDD\Event\DomainEventPublisher;

class File
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var VirusScanStatus
     */
    private $virusScanStatus;

    /**
     * @var VirusScanResult
     */
    private $virusScanResult;

    /**
     * @var \DateTime
     */
    private $enqueuedAt;

    /**
     * @var \DateTime
     */
    private $scanStartedAt;

    /**
     * @var \DateTime
     */
    private $scanFinishedAt;

    /**
     * @var string
     */
    private $scanId;

    /**
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->virusScanStatus = VirusScanStatus::buildNotScanned();
        $this->enqueuedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    public function startScan()
    {
        if (!$this->virusScanStatus->equals(VirusScanStatus::buildNotScanned())) {
            throw new ScanAlreadyStartedException();
        }
        $this->virusScanStatus = VirusScanStatus::buildScanStarted();
        $this->scanStartedAt = new \DateTime();
    }

    /**
     * @param VirusScanResult $virusScanResult
     * @param \DateTime|null $finishDate
     * @throws ScanHasNotStartedException
     */
    public function finishScan($virusScanResult, \DateTime $finishDate = null)
    {
        if ($this->virusScanStatus->equals(VirusScanStatus::buildNotScanned())) {
            throw new ScanHasNotStartedException();
        }
        $this->virusScanStatus = VirusScanStatus::buildScanned();
        $this->virusScanResult = $virusScanResult;
        $this->scanFinishedAt = $finishDate ? $finishDate : new \DateTime();
        DomainEventPublisher::instance()->publish(
          new VirusScanCompleted($this, $virusScanResult)
        );
    }

    /**
     * @return VirusScanStatus
     */
    public function virusScanStatus()
    {
        return $this->virusScanStatus;
    }

    /**
     * @return string
     */
    public function scanId()
    {
        return $this->scanId;
    }

    /**
     * @param string $scanId
     */
    public function setScanId($scanId)
    {
        $this->scanId = $scanId;
    }
}
