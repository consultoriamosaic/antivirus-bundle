<?php

namespace Antivirus\Domain\Antivirus;

class VirusScannableFile
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $originalName;

    /**
     * @var VirusScanStatus
     */
    private $virusScanStatus;

    /**
     * @var VirusScanResult
     */
    private $virusScanResult;

    /**
     * @param string $name
     * @param string $originalName
     * @param VirusScanStatus $virusScanStatus
     * @param VirusScanResult $virusScanResult
     */
    public function __construct(
      $name,
      $originalName,
      VirusScanStatus $virusScanStatus,
      VirusScanResult $virusScanResult = null
    ) {
        $this->name = $name;
        $this->originalName = $originalName;
        $this->virusScanStatus = $virusScanStatus;
        $this->virusScanResult = $virusScanResult;
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function originalName()
    {
        return $this->originalName;
    }

    /**
     * @return VirusScanStatus
     */
    public function virusScanStatus()
    {
        return $this->virusScanStatus;
    }

    /**
     * @return VirusScanResult
     */
    public function virusScanResult()
    {
        return $this->virusScanResult;
    }

    /**
     * @param VirusScanResult $virusScanResult
     * @return VirusScannableFile
     */
    public function changeVirusScanResult(VirusScanResult $virusScanResult)
    {
        return new self(
          $this->name(),
          $this->originalName(),
          $this->virusScanStatus(),
          $virusScanResult
        );
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->originalName();
    }

    public function changeVirusScanStatus($virusScanStatus)
    {
        return new self(
          $this->name(),
          $this->originalName(),
          $virusScanStatus,
          $this->virusScanResult()
        );
    }
}
