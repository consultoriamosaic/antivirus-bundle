<?php

namespace Antivirus\Domain\Antivirus;

use Mosaic\Common\DDD\Event\DomainEventInterface;

class VirusScanCompleted implements DomainEventInterface
{
    /**
     * @var File
     */
    private $file;

    /**
     * @var VirusScanResult
     */
    private $virusScanResult;

    /**
     * @var \DateTimeImmutable
     */
    private $occurredOn;

    /**
     * @param File $file
     * @param VirusScanResult $virusScanResult
     */
    public function __construct(File $file, VirusScanResult $virusScanResult)
    {
        $this->file = $file;
        $this->virusScanResult = $virusScanResult;
        $this->occurredOn = new \DateTimeImmutable();
    }

    /**
     * @return \Antivirus\Domain\Antivirus\File
     */
    public function file()
    {
        return $this->file;
    }

    /**
     * @return \Antivirus\Domain\Antivirus\VirusScanResult
     */
    public function virusScanResult()
    {
        return $this->virusScanResult;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function occurredOn()
    {
        return $this->occurredOn;
    }
}
