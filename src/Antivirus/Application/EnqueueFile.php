<?php

namespace Antivirus\Application;

use Antivirus\Domain\Antivirus\File;
use Antivirus\Domain\Antivirus\FileRepositoryInterface;

class EnqueueFile
{
    /**
     * @var FileRepositoryInterface
     */
    private $fileRepository;

    /**
     * @param FileRepositoryInterface $fileRepository
     */
    public function __construct(FileRepositoryInterface $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param string $name
     */
    public function __invoke($name)
    {
        $this->fileRepository->enqueue(new File($name));
    }
}
