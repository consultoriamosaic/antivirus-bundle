<?php

namespace Antivirus\Application;

use Antivirus\Domain\Antivirus\AsyncVirusScannerInterface;
use Antivirus\Domain\Antivirus\File;
use Antivirus\Domain\Antivirus\FileRepositoryInterface;
use Antivirus\Domain\Antivirus\VirusScanResult;

class GetFileScanResult
{
    /**
     * @var AsyncVirusScannerInterface
     */
    private $asyncVirusScanner;

    /**
     * @var FileRepositoryInterface
     */
    private $fileRepository;

    /**
     * @param AsyncVirusScannerInterface $asyncVirusScanner
     * @param FileRepositoryInterface $fileRepository
     */
    public function __construct(
      AsyncVirusScannerInterface $asyncVirusScanner,
      FileRepositoryInterface $fileRepository
    ) {
        $this->asyncVirusScanner = $asyncVirusScanner;
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param File $file
     * @return VirusScanResult
     */
    public function __invoke(File $file)
    {
        $virusScanResults = $this->asyncVirusScanner->getScanResult($file);
        $file->finishScan($virusScanResults);
        $this->fileRepository->save($file);
    }
}
