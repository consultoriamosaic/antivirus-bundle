<?php

namespace Antivirus\Application;

use Antivirus\Domain\Antivirus\EmptyFileQueueException;
use Antivirus\Domain\Antivirus\FileRepositoryInterface;

class ScanNextFileAsync
{
    /**
     * @var FileRepositoryInterface
     */
    private $fileRepository;

    /**
     * @var ScanFileAsync
     */
    private $scanFileAsyncService;

    /**
     * @param FileRepositoryInterface $fileRepository
     * @param ScanFileAsync $scanFileAsyncService
     */
    public function __construct(
      FileRepositoryInterface $fileRepository,
      ScanFileAsync $scanFileAsyncService
    ) {
        $this->fileRepository = $fileRepository;
        $this->scanFileAsyncService = $scanFileAsyncService;
    }

    /**
     * @throws EmptyFileQueueException
     */
    public function __invoke()
    {
        $file = $this->fileRepository->nextFileForScan();
        $this->scanFileAsyncService->__invoke($file);
    }
}
