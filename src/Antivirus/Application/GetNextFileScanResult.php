<?php

namespace Antivirus\Application;

use Antivirus\Domain\Antivirus\FileRepositoryInterface;

class GetNextFileScanResult
{
    const SCAN_TIME_IN_SECONDS = 300;

    /**
     * @var FileRepositoryInterface
     */
    private $fileRepository;

    /**
     * @var GetFileScanResult
     */
    private $getFileScanResultService;

    /**
     * @param FileRepositoryInterface $fileRepository
     * @param GetFileScanResult $getFileScanResultService
     */
    public function __construct(
      FileRepositoryInterface $fileRepository,
      GetFileScanResult $getFileScanResultService
    ) {
        $this->fileRepository = $fileRepository;
        $this->getFileScanResultService = $getFileScanResultService;
    }

    public function __invoke()
    {
        $file = $this->fileRepository->nextFileForReport(self::SCAN_TIME_IN_SECONDS);
        $this->getFileScanResultService->__invoke($file);
    }
}