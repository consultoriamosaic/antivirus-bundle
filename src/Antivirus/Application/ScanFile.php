<?php

namespace Antivirus\Application;

use Antivirus\Domain\Antivirus\File;
use Antivirus\Domain\Antivirus\VirusScannerInterface;
use Antivirus\Domain\Antivirus\VirusScanResult;

class ScanFile
{
    /**
     * @var VirusScannerInterface
     */
    private $virusScanner;

    /**
     * @param VirusScannerInterface $virusScanner
     */
    public function __construct($virusScanner)
    {
        $this->virusScanner = $virusScanner;
    }

    /**
     * @param File $file
     * @return VirusScanResult
     */
    public function __invoke($file)
    {
        $file->startScan();
        $virusScanResult = $this->virusScanner->scanFile($file);
        $file->finishScan($virusScanResult);

        return $virusScanResult;
    }
}
