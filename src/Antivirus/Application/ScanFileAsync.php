<?php

namespace Antivirus\Application;

use Antivirus\Domain\Antivirus\AsyncVirusScannerInterface;
use Antivirus\Domain\Antivirus\File;
use Antivirus\Domain\Antivirus\FileRepositoryInterface;

class ScanFileAsync
{
    /**
     * @var AsyncVirusScannerInterface
     */
    private $asyncVirusScanner;

    /**
     * @var FileRepositoryInterface
     */
    private $fileRepository;

    /**
     * @param AsyncVirusScannerInterface $asyncVirusScanner
     * @param FileRepositoryInterface $fileRepository
     */
    public function __construct(
      AsyncVirusScannerInterface $asyncVirusScanner,
      FileRepositoryInterface $fileRepository
    ) {
        $this->asyncVirusScanner = $asyncVirusScanner;
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param File $file
     */
    public function __invoke(File $file)
    {
        $file->startScan();
        $scanId = $this->asyncVirusScanner->scanFileAsync($file);
        $file->setScanId($scanId);
        $this->fileRepository->save($file);
    }
}