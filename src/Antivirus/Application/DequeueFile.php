<?php

namespace Antivirus\Application;

use Antivirus\Domain\Antivirus\File;
use Antivirus\Domain\Antivirus\FileRepositoryInterface;

class DequeueFile
{
    /**
     * @var FileRepositoryInterface
     */
    private $fileRepository;

    /**
     * @param FileRepositoryInterface $fileRepository
     */
    public function __construct(FileRepositoryInterface $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    /**
     * @return File
     */
    public function __invoke()
    {
        return $this->fileRepository->nextFileForScan();
    }
}
