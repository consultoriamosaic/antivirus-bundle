<?php

namespace Antivirus\Application;

use Antivirus\Domain\Antivirus\FileRepositoryInterface;

class GetReportQueueLength
{
    /**
     * @var FileRepositoryInterface
     */
    private $fileRepository;

    /**
     * @param FileRepositoryInterface $fileRepository
     */
    public function __construct(FileRepositoryInterface $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    /**
     * @return int
     */
    public function __invoke()
    {
        return $this->fileRepository->reportQueueLength();
    }
}
