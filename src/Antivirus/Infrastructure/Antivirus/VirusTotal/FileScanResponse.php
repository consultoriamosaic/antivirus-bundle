<?php

namespace Antivirus\Infrastructure\Antivirus\VirusTotal;

class FileScanResponse
{
    /**
     * @var int
     */
    private $code;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $resource;

    /**
     * @var string
     */
    private $scanId;

    /**
     * @var string
     */
    private $permalink;

    /**
     * @var string
     */
    private $sha256;

    /**
     * @var string
     */
    private $sha1;

    /**
     * @var string
     */
    private $md5;

    /**
     * @param int $code
     * @param string $message
     * @param string $resource
     * @param string $scanId
     * @param string $permalink
     * @param string $sha256
     * @param string $sha1
     * @param string $md5
     */
    public function __construct(
      $code,
      $message,
      $resource,
      $scanId,
      $permalink,
      $sha256,
      $sha1,
      $md5
    ) {
        $this->code = $code;
        $this->message = $message;
        $this->resource = $resource;
        $this->scanId = $scanId;
        $this->permalink = $permalink;
        $this->sha256 = $sha256;
        $this->sha1 = $sha1;
        $this->md5 = $md5;
    }

    public static function fromJsonResponse($jsonResponse)
    {
        $data = json_decode($jsonResponse, true);
        return new static(
          $data['response_code'],
          $data['verbose_msg'],
          $data['resource'],
          $data['scan_id'],
          $data['permalink'],
          $data['sha256'],
          $data['sha1'],
          $data['md5']
        );
    }

    /**
     * @return int
     */
    public function code()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function message()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function resource()
    {
        return $this->resource;
    }

    /**
     * @return string
     */
    public function scanId()
    {
        return $this->scanId;
    }

    /**
     * @return string
     */
    public function permalink()
    {
        return $this->permalink;
    }

    /**
     * @return string
     */
    public function sha256()
    {
        return $this->sha256;
    }

    /**
     * @return string
     */
    public function sha1()
    {
        return $this->sha1;
    }

    /**
     * @return string
     */
    public function md5()
    {
        return $this->md5;
    }

    /**
     * @return bool
     */
    public function hasError()
    {
        return $this->code() === 0;
    }
}
