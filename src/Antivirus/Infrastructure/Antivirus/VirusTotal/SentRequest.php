<?php

namespace Antivirus\Infrastructure\Antivirus\VirusTotal;

class SentRequest
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var \DateTime
     */
    private $requestedAt;

    /**
     * @param string $endpoint
     * @param \DateTime|null $requestedAt
     */
    public function __construct($endpoint, $requestedAt = null)
    {
        $this->endpoint = $endpoint;
        $this->requestedAt = $requestedAt ? $requestedAt : new \DateTime();
    }

    /**
     * @return int
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function endpoint()
    {
        return $this->endpoint;
    }

    /**
     * @return \DateTime
     */
    public function requestedAt()
    {
        return $this->requestedAt;
    }
}