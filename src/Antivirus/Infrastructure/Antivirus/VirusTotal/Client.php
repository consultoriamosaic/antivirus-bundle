<?php

namespace Antivirus\Infrastructure\Antivirus\VirusTotal;

class Client
{
    const API_ENDPOINT = 'https://www.virustotal.com/vtapi/v2/';

    const REQUEST_LIMIT_PER_MINUTE = 4;

    const FILE_SCAN_ENDPOINT = 'file/scan';

    const FILE_REPORT_ENDPOINT = 'file/report';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @param \GuzzleHttp\Client $client
     * @param string $apiKey
     */
    public function __construct(\GuzzleHttp\Client $client, $apiKey)
    {
        $this->client = $client;
        $this->apiKey = $apiKey;
    }

    /**
     * @param string $file
     * @return FileScanResponse
     * @throws RequestLimitReachedException
     */
    public function scan($file)
    {
        $data = $this->client->post( self::API_ENDPOINT . 'file/scan', [
          'multipart' => [
            [
              'name' => 'apikey',
              'contents' => $this->apiKey
            ],
            [
              'name' => 'file',
              'contents' => fopen($file,'r'),
              'filename' => $file
            ]
          ]
        ]);

        return FileScanResponse::fromJsonResponse($data->getBody()->getContents());
    }

    /**
     * @param $resource
     * @return FileReportResponse
     * @throws RequestLimitReachedException
     */
    public function report($resource)
    {
        $data = $this->client->post( self::API_ENDPOINT . 'file/report', [
          'form_params' => [
            'apikey' => $this->apiKey,
            'resource' => $resource
          ]
        ]);

        return FileReportResponse::fromJsonResponse($data->getBody()->getContents());
    }
}
