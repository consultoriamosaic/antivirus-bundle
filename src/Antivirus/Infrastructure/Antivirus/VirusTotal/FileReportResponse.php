<?php

namespace Antivirus\Infrastructure\Antivirus\VirusTotal;

class FileReportResponse
{
    const PENDING_MESSAGE = 'The requested resource is not among the finished, queued or pending scans';
    const INVALID_RESOURCE_MESSAGE = 'Invalid resource, check what you are submitting';
    const QUEUED_MESSAGE = 'Your resource is queued for analysis';
    const FINISHED_MESSAGE = 'Scan finished, information embedded';

    /**
     * @var int
     */
    private $code;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $resource;

    /**
     * @var string
     */
    private $scanId;

    /**
     * @var string
     */
    private $md5;

    /**
     * @var string
     */
    private $sha1;

    /**
     * @var string
     */
    private $sha256;

    /**
     * @var \DateTime
     */
    private $scanDate;

    /**
     * @var int
     */
    private $positives;

    /**
     * @var int
     */
    private $total;

    /**
     * @var array
     */
    private $scans;

    /**
     * @var string
     */
    private $permalink;

    /**
     * VirusTotalFileReportResponse constructor.
     * @param int $code
     * @param string $message
     * @param string $resource
     * @param string $scanId
     * @param string $md5
     * @param string $sha1
     * @param string $sha256
     * @param \DateTime $scanDate
     * @param int $positives
     * @param int $total
     * @param array $scans
     * @param string $permalink
     */
    public function __construct(
      $code,
      $message,
      $resource,
      $scanId = null,
      $md5 = null,
      $sha1 = null,
      $sha256 = null,
      \DateTime $scanDate = null,
      $positives = null,
      $total = null,
      array $scans = null,
      $permalink = null
    ) {
        $this->code = $code;
        $this->message = $message;
        $this->resource = $resource;
        $this->scanId = $scanId;
        $this->md5 = $md5;
        $this->sha1 = $sha1;
        $this->sha256 = $sha256;
        $this->scanDate = $scanDate;
        $this->positives = $positives;
        $this->total = $total;
        $this->scans = $scans;
        $this->permalink = $permalink;
    }

    /**
     * @param string $jsonResponse
     * @return static
     */
    public static function fromJsonResponse($jsonResponse)
    {
        $data = json_decode($jsonResponse, true);
        return new static(
          $data['response_code'],
          $data['verbose_msg'],
          $data['resource'],
          $data['scan_id'],
          isset($data['md5']) ? $data['md5'] : null,
          isset($data['sha1']) ? $data['sha1'] : null,
          isset($data['sha256']) ? $data['sha256'] : null,
          isset($data['scan_date']) ? new \DateTime($data['scan_date']) : null,
          isset($data['positives']) ? $data['positives'] : null,
          isset($data['total']) ? $data['total'] : null,
          isset($data['scans']) ? $data['scans'] : null,
          isset($data['permalink']) ? $data['permalink'] : null
        );
    }

    /**
     * @return int
     */
    public function code()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function message()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function resource()
    {
        return $this->resource;
    }

    /**
     * @return string
     */
    public function scanId()
    {
        return $this->scanId;
    }

    /**
     * @return string
     */
    public function md5()
    {
        return $this->md5;
    }

    /**
     * @return string
     */
    public function sha1()
    {
        return $this->sha1;
    }

    /**
     * @return string
     */
    public function sha256()
    {
        return $this->sha256;
    }

    /**
     * @return \DateTime
     */
    public function scanDate()
    {
        return $this->scanDate;
    }

    /**
     * @return int
     */
    public function positives()
    {
        return $this->positives;
    }

    /**
     * @return int
     */
    public function total()
    {
        return $this->total;
    }

    /**
     * @return array
     */
    public function scans()
    {
        return $this->scans;
    }

    /**
     * @return string
     */
    public function permalink()
    {
        return $this->permalink;
    }

    /**
     * @return bool
     */
    public function isPendingOrQueued()
    {
        return $this->code() === 0 && $this->message() === self::PENDING_MESSAGE ||
          $this->code() === -2 && $this->message() === self::QUEUED_MESSAGE;
    }

    /**
     * @return bool
     */
    public function isInvalidResource()
    {
        return $this->code() === 0 && $this->message() === self::INVALID_RESOURCE_MESSAGE;
    }

    public function isFinished()
    {
        return $this->code() === 1 && $this->message() === self::FINISHED_MESSAGE;
    }
}