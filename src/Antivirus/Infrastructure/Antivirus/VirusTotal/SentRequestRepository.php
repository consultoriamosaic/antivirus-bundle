<?php

namespace Antivirus\Infrastructure\Antivirus\VirusTotal;

interface SentRequestRepository
{
    /**
     * @param SentRequest $virusTotalSentRequest
     */
    public function add(SentRequest $virusTotalSentRequest);

    /**
     * @param \DateTime $dateTime
     * @return int
     */
    public function countSentRequestSince(\DateTime $dateTime);
}
