<?php

namespace Antivirus\Infrastructure\EventSubscriber\Domain;

use Antivirus\Application\EnqueueFile;
use Antivirus\Domain\Antivirus\VirusScanRequested;
use Mosaic\Common\DDD\Event\DomainEventInterface;
use Mosaic\Common\DDD\Event\DomainEventSubscriberInterface;

class VirusScanRequestSubscriber implements DomainEventSubscriberInterface
{
    /**
     * @var EnqueueFile
     */
    private $enqueueFileService;

    /**
     * @param EnqueueFile $enqueueFileService
     */
    public function __construct(EnqueueFile $enqueueFileService)
    {
        $this->enqueueFileService = $enqueueFileService;
    }


    /**
     * @param DomainEventInterface $event
     * @return bool
     */
    public function isSubscribedTo(DomainEventInterface $event)
    {
        return VirusScanRequested::class === get_class($event);
    }

    /**
     * @param VirusScanRequested $event
     */
    public function handle($event)
    {
        $this->enqueueFileService->__invoke($event->filename());
    }
}
