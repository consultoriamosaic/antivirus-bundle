<?php

namespace Antivirus\Infrastructure\Persistence\Repository\Antivirus\VirusTotal;

use Antivirus\Infrastructure\Antivirus\VirusTotal\SentRequest;
use Antivirus\Infrastructure\Antivirus\VirusTotal\SentRequestRepository;
use Doctrine\ORM\EntityRepository;

class DoctrineSentRequestRepository extends EntityRepository implements SentRequestRepository
{
    /**
     * @param SentRequest $virusTotalSentRequest
     */
    public function add(SentRequest $virusTotalSentRequest)
    {
        $this->_em->persist($virusTotalSentRequest);
        $this->_em->flush($virusTotalSentRequest);
    }

    /**
     * @param \DateTime $dateTime
     * @return int
     */
    public function countSentRequestSince(\DateTime $dateTime)
    {
        $qb = $this->createQueryBuilder('sr');
        $qb->select($qb->expr()->count('sr'))
            ->where('sr.requestedAt >= :requestedAt')
            ->setParameter(':requestedAt', $dateTime->format('Y-m-d H:i:s'));

        return $qb->getQuery()->getSingleScalarResult();
    }
}
