<?php

namespace Antivirus\Infrastructure\Persistence\Repository;

use Antivirus\Domain\Antivirus\EmptyFileQueueException;
use Antivirus\Domain\Antivirus\File;
use Antivirus\Domain\Antivirus\FileRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class DoctrineFileRepository extends EntityRepository implements FileRepositoryInterface
{
    /**
     * @param File $file
     */
    public function enqueue(File $file)
    {
        $this->_em->persist($file);
        $this->save($file);
    }

    /**
     * @param File $file
     */
    public function save(File $file)
    {
        $this->_em->flush($file);
    }

    /**
     * @return File
     * @throws EmptyFileQueueException
     */
    public function nextFileForScan()
    {
        if ($this->scanQueueLength() === 0) {
            throw new EmptyFileQueueException();
        }

        $file = $this->findOneBy(['scanStartedAt' => null], ['enqueuedAt' => 'ASC']);

        if (!$file instanceof File) {
            throw new EmptyFileQueueException();
        }

        return $file;
    }

    /**
     * @param int $scanTimeInSeconds
     * @return File
     * @throws EmptyFileQueueException
     */
    public function nextFileForReport($scanTimeInSeconds)
    {
        if ($this->reportQueueLength() === 0) {
            throw new EmptyFileQueueException();
        }

        $qb = $this->createQueryBuilder('f')
          ->where('f.scanStartedAt IS NOT NULL')
          ->andWhere('f.scanFinishedAt IS NULL')
          ->andWhere('TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP(), f.scanStartedAt)) >= :scanTimeInSeconds')
          ->setParameter(':scanTimeInSeconds', $scanTimeInSeconds)
          ->orderBy('f.scanStartedAt', 'ASC')
          ->setMaxResults(1);

        $file = $qb->getQuery()->getOneOrNullResult();

        if (!$file instanceof File) {
            throw new EmptyFileQueueException();
        }

        return $file;
    }

    /**
     * @return int
     */
    public function scanQueueLength()
    {
        $qb = $this->createQueryBuilder('f');

        $qb->select($qb->expr()->count('f'))
            ->where('f.scanStartedAt IS NULL');

        return (int)$qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @return int
     */
    public function reportQueueLength()
    {
        $qb = $this->createQueryBuilder('f');

        $qb->select($qb->expr()->count('f'))
          ->where('f.scanStartedAt IS NOT NULL')
          ->andWhere('f.scanFinishedAt IS NULL');

        return (int)$qb->getQuery()->getSingleScalarResult();
    }
}
