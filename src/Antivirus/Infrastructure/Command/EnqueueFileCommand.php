<?php

namespace Antivirus\Infrastructure\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EnqueueFileCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('antivirus:queue:enqueue')
          ->setDescription('Enqueues a file for antivirus scan.')
          ->addArgument('fileName', InputArgument::REQUIRED, 'The file name');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fileName = $input->getArgument('fileName');
        $success = true;

        try {
            $this->getContainer()->get('antivirus.service.enqueue_file')
              ->__invoke($fileName);
        } catch (\Exception $e) {
            $success = false;
            $output->writeln('Error: ' . $e->getMessage());
        }

        if ($success) {
            $output->writeln('File enqueued successfully.');
        }
    }
}
