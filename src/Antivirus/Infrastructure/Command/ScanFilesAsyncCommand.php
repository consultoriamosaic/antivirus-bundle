<?php

namespace Antivirus\Infrastructure\Command;

use Antivirus\Domain\Antivirus\EmptyFileQueueException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScanFilesAsyncCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('antivirus:queue:async-scan')
            ->setDescription('Scans enqueued files');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $scanNextFileAsync = $this->getContainer()->get('antivirus.service.scan_next_file_async');
        $getNextFileScanResult = $this->getContainer()->get('antivirus.service.get_next_file_scan_result');
        $getScanQueueLength = $this->getContainer()->get('antivirus.service.get_scan_queue_length');
        $getReportQueueLength = $this->getContainer()->get('antivirus.service.get_report_queue_length');

        $output->writeln('Starting asynchronous file scanner.');

        // Start scan of all enqueued files
        while ($getScanQueueLength() > 0) {
            try {
                $scanNextFileAsync();
            } catch (EmptyFileQueueException $e) {
                $output->writeln('Scan queue is empty.');
                break;
            } catch (\Exception $e) {
                $output->writeln($e->getMessage());
                break;
            }
        }

        // try to get the report
        while ($getReportQueueLength() > 0) {
            try {
                $getNextFileScanResult();
            } catch (EmptyFileQueueException $e) {
                $output->writeln('Report queue is empty.');
                break;
            } catch (\Exception $e) {
                $output->writeln($e->getMessage());
                break;
            }
        }
    }
}