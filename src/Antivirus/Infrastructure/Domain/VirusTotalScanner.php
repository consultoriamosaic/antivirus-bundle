<?php

namespace Antivirus\Infrastructure\Domain;

use Antivirus\Domain\Antivirus\AsyncVirusScannerInterface;
use Antivirus\Domain\Antivirus\File;
use Antivirus\Domain\Antivirus\VirusScanResult;
use Antivirus\Infrastructure\Antivirus\VirusTotal\Client;
use Antivirus\Infrastructure\Antivirus\VirusTotal\InvalidResourceException;
use Antivirus\Infrastructure\Antivirus\VirusTotal\PendingScanException;
use Antivirus\Infrastructure\Antivirus\VirusTotal\RequestException;
use Antivirus\Infrastructure\Antivirus\VirusTotal\RequestLimitReachedException;
use Antivirus\Infrastructure\Antivirus\VirusTotal\ScanException;
use Antivirus\Infrastructure\Antivirus\VirusTotal\SentRequest;
use Antivirus\Infrastructure\Antivirus\VirusTotal\SentRequestRepository;

class VirusTotalScanner implements AsyncVirusScannerInterface
{
    /**
     * @var Client
     */
    private $virusTotalApi;

    /**
     * @var SentRequestRepository
     */
    private $sentRequestHistory;

    /**
     * @var string
     */
    private $filesDirectory;

    /**
     * @param Client $virusTotalApi
     * @param SentRequestRepository $sentRequestHistory
     * @param string $filesDirectory
     */
    public function __construct($virusTotalApi, $sentRequestHistory, $filesDirectory)
    {
        $this->virusTotalApi = $virusTotalApi;
        $this->sentRequestHistory = $sentRequestHistory;
        $this->filesDirectory = $filesDirectory;
    }

    /**
     * @param File $file
     * @return string
     * @throws \Antivirus\Infrastructure\Antivirus\VirusTotal\RequestException
     * @throws \Antivirus\Infrastructure\Antivirus\VirusTotal\RequestLimitReachedException
     * @throws \Antivirus\Infrastructure\Antivirus\VirusTotal\ScanException
     */
    public function scanFileAsync(File $file)
    {
        $this->assertRequestLimitSince();

        $filePath = $this->filesDirectory . DIRECTORY_SEPARATOR . $file->name();
        try {
            $virusTotalResponse = $this->virusTotalApi->scan($filePath);
            $this->sentRequestHistory->add(new SentRequest(Client::FILE_SCAN_ENDPOINT));
        } catch (\Exception $e) {
            throw new RequestException();
        }

        if ($virusTotalResponse->hasError()) {
            throw new ScanException();
        }
        return $virusTotalResponse->scanId();
    }

    /**
     * @param File $file
     * @return VirusScanResult
     * @throws InvalidResourceException
     * @throws PendingScanException
     * @throws RequestException
     * @throws \Exception
     */
    public function getScanResult(File $file)
    {
        $this->assertRequestLimitSince();

        try {
            $virusTotalFileReportResponse = $this->virusTotalApi->report($file->scanId());
            $this->sentRequestHistory->add(new SentRequest(Client::FILE_REPORT_ENDPOINT));
        } catch (\Exception $e) {
            throw new RequestException($e->getMessage());
        }

        if ($virusTotalFileReportResponse->isPendingOrQueued()) {
            throw new PendingScanException($virusTotalFileReportResponse->message());
        }
        if ($virusTotalFileReportResponse->isInvalidResource()) {
            throw new InvalidResourceException($virusTotalFileReportResponse->message());
        }

        if (!$virusTotalFileReportResponse->isFinished()) {
            throw new \Exception($virusTotalFileReportResponse->message());
        }

        if ($virusTotalFileReportResponse->positives() === 0) {
            return VirusScanResult::buildNotInfectedFileScanResult();
        } else {
            return VirusScanResult::buildInfectedFileScanResult();
        }
    }

    /**
     * @throws \Antivirus\Infrastructure\Antivirus\VirusTotal\RequestLimitReachedException
     */
    private function assertRequestLimitSince()
    {
        $lastMinuteRequestCount = $this->sentRequestHistory
          ->countSentRequestSince(new \DateTime('now -1 minute'));

        if ($lastMinuteRequestCount >= Client::REQUEST_LIMIT_PER_MINUTE) {
            throw new RequestLimitReachedException();
        }
    }
}
