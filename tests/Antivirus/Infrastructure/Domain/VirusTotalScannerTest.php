<?php

namespace tests\Antivirus\Infrastructure\Domain;

use Antivirus\Domain\Antivirus\File;
use Antivirus\Infrastructure\Antivirus\VirusTotal\Client;
use Antivirus\Infrastructure\Antivirus\VirusTotal\FileReportResponse;
use Antivirus\Infrastructure\Antivirus\VirusTotal\FileScanResponse;
use Antivirus\Infrastructure\Antivirus\VirusTotal\SentRequestRepository;
use Antivirus\Infrastructure\Domain\VirusTotalScanner;

class VirusTotalScannerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function itShouldRequestFileScanSuccessfully()
    {
        $file = $this->createMock(File::class);

        $virusTotalApi = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $virusTotalScanResponse = $this->getMockBuilder(FileScanResponse::class)
          ->disableOriginalConstructor()
          ->getMock();

        $virusTotalScanResponse->method('hasError')->willReturn(false);

        $sentRequestHistory = $this->createMock(SentRequestRepository::class);

        $virusTotalApi->method('scan')->willReturn($virusTotalScanResponse);

        $virusTotalScanner = new VirusTotalScanner($virusTotalApi, $sentRequestHistory, '');

        $virusTotalScanner->scanFileAsync($file);
    }

    /**
     * @test
     * @expectedException \Antivirus\Infrastructure\Antivirus\VirusTotal\ScanException
     */
    public function itShouldFailWhenRequestingAFileScan()
    {
        $file = $this->createMock(File::class);

        $virusTotalApi = $this->getMockBuilder(Client::class)
          ->disableOriginalConstructor()
          ->getMock();

        $virusTotalScanResponse = $this->getMockBuilder(FileScanResponse::class)
          ->disableOriginalConstructor()
          ->getMock();

        $virusTotalScanResponse->method('hasError')->willReturn(true);

        $sentRequestHistory = $this->createMock(SentRequestRepository::class);

        $virusTotalApi->method('scan')->willReturn($virusTotalScanResponse);

        $virusTotalScanner = new VirusTotalScanner($virusTotalApi, $sentRequestHistory, '');

        $virusTotalScanner->scanFileAsync($file);
    }

    /**
     * @test
     */
    public function itShouldGetNotInfectedFileScanResult()
    {
        $file = $this->createMock(File::class);

        $virusTotalApi = $this->getMockBuilder(Client::class)
          ->disableOriginalConstructor()
          ->getMock();

        $virusTotalFileReportResponse = $this->getMockBuilder(FileReportResponse::class)
          ->disableOriginalConstructor()
          ->getMock();

        $virusTotalFileReportResponse->method('code')->willReturn(1);
        $virusTotalFileReportResponse->method('positives')->willReturn(0);
        $virusTotalFileReportResponse->method('isPendingOrQueued')->willReturn(false);
        $virusTotalFileReportResponse->method('isInvalidResource')->willReturn(false);
        $virusTotalFileReportResponse->method('isFinished')->willReturn(true);

        $sentRequestHistory = $this->createMock(SentRequestRepository::class);

        $virusTotalApi->method('report')->willReturn($virusTotalFileReportResponse);

        $virusTotalScanner = new VirusTotalScanner($virusTotalApi, $sentRequestHistory, '');

        $virusScanResult = $virusTotalScanner->getScanResult($file);

        $this->assertFalse($virusScanResult->hasVirus());
    }


    /**
     * @test
     */
    public function itShouldGetInfectedFileScanResult()
    {
        $file = $this->createMock(File::class);

        $virusTotalApi = $this->getMockBuilder(Client::class)
          ->disableOriginalConstructor()
          ->getMock();

        $virusTotalFileReportResponse = $this->getMockBuilder(FileReportResponse::class)
          ->disableOriginalConstructor()
          ->getMock();

        $virusTotalFileReportResponse->method('code')->willReturn(1);
        $virusTotalFileReportResponse->method('positives')->willReturn(1);
        $virusTotalFileReportResponse->method('isPendingOrQueued')->willReturn(false);
        $virusTotalFileReportResponse->method('isInvalidResource')->willReturn(false);
        $virusTotalFileReportResponse->method('isFinished')->willReturn(true);

        $sentRequestHistory = $this->createMock(SentRequestRepository::class);

        $virusTotalApi->method('report')->willReturn($virusTotalFileReportResponse);

        $virusTotalScanner = new VirusTotalScanner($virusTotalApi, $sentRequestHistory, '');

        $virusScanResult = $virusTotalScanner->getScanResult($file);

        $this->assertTrue($virusScanResult->hasVirus());
    }

    /**
     * @test
     * @expectedException \Antivirus\Infrastructure\Antivirus\VirusTotal\InvalidResourceException
     */
    public function givenAnInvalidResourceIdItShouldThrowAnException()
    {
        $file = $this->createMock(File::class);

        $virusTotalApi = $this->getMockBuilder(Client::class)
          ->disableOriginalConstructor()
          ->getMock();

        $virusTotalFileReportResponse = $this->getMockBuilder(FileReportResponse::class)
          ->disableOriginalConstructor()
          ->getMock();

        $virusTotalFileReportResponse->method('isPendingOrQueued')->willReturn(false);
        $virusTotalFileReportResponse->method('isInvalidResource')->willReturn(true);

        $sentRequestHistory = $this->createMock(SentRequestRepository::class);

        $virusTotalApi->method('report')->willReturn($virusTotalFileReportResponse);

        $virusTotalScanner = new VirusTotalScanner($virusTotalApi, $sentRequestHistory, '');

        $virusTotalScanner->getScanResult($file);
    }

    /**
     * @test
     * @expectedException \Antivirus\Infrastructure\Antivirus\VirusTotal\PendingScanException
     */
    public function givenAPendingForScanResourceItShouldThrowAnException()
    {
        $file = $this->createMock(File::class);

        $virusTotalApi = $this->getMockBuilder(Client::class)
          ->disableOriginalConstructor()
          ->getMock();

        $virusTotalFileReportResponse = $this->getMockBuilder(FileReportResponse::class)
          ->disableOriginalConstructor()
          ->getMock();

        $sentRequestHistory = $this->createMock(SentRequestRepository::class);

        $virusTotalFileReportResponse->method('isPendingOrQueued')->willReturn(true);
        $virusTotalFileReportResponse->method('isInvalidResource')->willReturn(false);

        $virusTotalApi->method('report')->willReturn($virusTotalFileReportResponse);

        $virusTotalScanner = new VirusTotalScanner($virusTotalApi, $sentRequestHistory, '');

        $virusTotalScanner->getScanResult($file);
    }
}
