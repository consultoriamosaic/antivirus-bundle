<?php

namespace tests\Antivirus\Domain;

use Antivirus\Domain\Antivirus\File;
use Antivirus\Domain\Antivirus\VirusScanResult;

class FileTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @expectedException \Antivirus\Domain\Antivirus\ScanAlreadyStartedException
     */
    public function scanCanNotBeStartedMultipleTimes()
    {
        $file = new File('test.txt');
        $file->startScan();
        $file->startScan();
    }

    /**
     * @test
     * @expectedException \Antivirus\Domain\Antivirus\ScanHasNotStartedException
     */
    public function scanCanNotBeFinishedIfItHasNotStarted()
    {
        $file = new File('test.txt');
        $virusScanResult = $this->createMock(VirusScanResult::class);
        $file->finishScan($virusScanResult);
    }
}
