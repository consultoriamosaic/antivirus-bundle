<?php

namespace tests\Antivirus\Application;

use Antivirus\Application\ScanFile;
use Antivirus\Domain\Antivirus\File;
use Antivirus\Domain\Antivirus\VirusScannerInterface;
use Antivirus\Domain\Antivirus\VirusScanResult;

class ScanFileTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function whenScanFileExecutedFileShouldHaveScanStarted()
    {
        $fileMock = $this->getMockBuilder(File::class)
          ->setMethods(['startScan', 'finishScan'])
          ->disableOriginalConstructor()
          ->getMock();

        $fileMock->expects($this->once())
          ->method('startScan');

        $fileMock->expects($this->once())
          ->method('finishScan');

        $asyncVirusScannerMock = $this->createMock(VirusScannerInterface::class);

        $scanFileAsyncService = new ScanFile($asyncVirusScannerMock);

        $scanFileAsyncService->__invoke($fileMock);
    }

    /**
     * @test
     */
    public function givenAnInfectedFileItShouldFindAVirus()
    {
        $file = $this->createMock(File::class);

        $virusScannerStub = $this->createMock(VirusScannerInterface::class);
        $virusScannerStub->method('scanFile')
          ->willReturn(VirusScanResult::buildInfectedFileScanResult());

        $scanFileService = new ScanFile($virusScannerStub);

        $scanResult = $scanFileService->__invoke($file);

        $this->assertTrue($scanResult->hasVirus());
    }

    /**
     * @test
     */
    public function givenANotInfectedFileItShouldNotFindAVirus()
    {
        $file = $this->createMock(File::class);

        $virusScannerStub = $this->createMock(VirusScannerInterface::class);
        $virusScannerStub->method('scanFile')
          ->willReturn(VirusScanResult::buildNotInfectedFileScanResult());

        $scanFileService = new ScanFile($virusScannerStub);

        $scanResult = $scanFileService->__invoke($file);

        $this->assertFalse($scanResult->hasVirus());
    }
}
