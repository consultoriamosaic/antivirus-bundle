<?php

namespace tests\Antivirus\Application;

use Antivirus\Application\ScanFileAsync;
use Antivirus\Domain\Antivirus\AsyncVirusScannerInterface;
use Antivirus\Domain\Antivirus\File;
use Antivirus\Domain\Antivirus\FileRepositoryInterface;

class ScanFileAsyncTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function whenScanFileAsyncExecutedFileShouldHaveScanStarted()
    {
        $fileRepository = $this->createMock(FileRepositoryInterface::class);

        $fileMock = $this->getMockBuilder(File::class)
            ->setMethods(['startScan'])
            ->disableOriginalConstructor()
            ->getMock();

        $fileMock->expects($this->once())
            ->method('startScan');

        $asyncVirusScannerMock = $this->createMock(AsyncVirusScannerInterface::class);

        $scanFileAsyncService = new ScanFileAsync($asyncVirusScannerMock, $fileRepository);

        $scanFileAsyncService->__invoke($fileMock);
    }
}
